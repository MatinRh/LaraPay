<?php
return
    [
        'request' => [
            'min' => 1000,
            'max' => 100000000
        ],
        'verify' => [
            'timeout' => 15
        ]
    ];