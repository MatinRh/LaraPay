<?php


namespace Tests\Feature\Controllers;


use Tests\TestCase;

class UsersControllerTest extends TestCase
{
    /** @test */
    public function it_can_delete_user()
    {
        $response = $this->get('/admin/users/delete');
        $response->assertStatus(200)->assertViewHas('users');
    }
}