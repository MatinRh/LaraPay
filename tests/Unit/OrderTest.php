<?php


namespace Tests\Unit;


use App\Product;
use PHPUnit\Framework\TestCase;
use App\Order;
class OrderTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

    }
    /** @test */
    public function an_order_has_many_products()
    {
        $order = new Order;

        $order->add(new Product('Galaxy S9','Mobile',1000));
        $order->add(new Product('Iphone x','Mobile',2000));

        $this->assertCount(2,$order->getProducts());

    }
    /** @test */
    public function an_order_can_calculate_total_price_of_its_products()
    {
        $order = new Order;

        $order->add(new Product('Galaxy S9','Mobile',1000));
        $order->add(new Product('Iphone x','Mobile',2000));

        $this->assertEquals(3000,$order->getTotalPrice());
    }
    /** @test */
    public function an_order_has_payment_url()
    {
        $order = new Order;

        $order->add(new Product('Galaxy S9','Mobile',1000));
        $order->add(new Product('Iphone x','Mobile',2000));

        $this->assertNotEmpty($order->getPaymentUrl());
    }

    public function tearDown()
    {

        parent::tearDown();
    }
}