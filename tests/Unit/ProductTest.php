<?php


namespace Tests\Unit;


use PHPUnit\Framework\TestCase;
use App\Product;
class ProductTest extends TestCase
{
   /** @test */
    public function a_product_has_a_name()
    {
        $product =  $this->makeProduct();
        $this->assertEquals('Galaxy S9',$product->getName());
    }

    /** @test */
    public function a_product_has_a_category()
    {
        $product =  $this->makeProduct();
        $this->assertEquals('Mobile',$product->getCategory());
    }
    /** @test */
    public function a_product_has_a_price()
    {
        $product = $this->makeProduct();
        $this->assertEquals(1000,$product->getPrice());
    }

    private function makeProduct()
    {
        return new Product('Galaxy S9','Mobile',1000);
    }

}