<?php


namespace App;


class Product
{
    private $name;
    private $category;
    private $price;

    public function __construct($name,$category,$price)
    {

        $this->name = $name;
        $this->category = $category;
        $this->price = $price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getPrice()
    {
        return $this->price;
    }

}