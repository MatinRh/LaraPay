<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GatewayReport extends Model
{
    protected $primaryKey = 'gateway_report_id';

    protected $guarded = ['gateway_report_id'];

    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'gateway_report_gateway_id');
    }
}
