<?php


namespace App\Services\User;


use App\Events\User\UserRegistered;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Notification\Facade\Notification;
use App\Services\Notification\NotificationService;
use App\Services\Notification\NotificationType;

class UserCreateService
{
    /**
     * @var array
     */
    private $userData;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(array $userData)
    {

        $this->userData = $userData;
        $this->userRepository = resolve(UserRepositoryInterface::class);
    }

    public function perform()
    {
        $newUser = $this->usersRepository->store([
            'name' => $this->userData['name'],
            'email' => $this->userData['email'],
            'password' => $this->userData['password']
        ]);
        if (intval($newUser->id)) {
            event(new UserRegistered($newUser));
//            Notification::setType(NotificationType::SMS)->send([
//                'to' => '09123456789',
//                'text' => 'سلام ثبت نام شما انجام شد'
//            ]);
            return true;
        }
        return false;
    }
}