<?php


namespace App\Services\Gateway;


use App\Repositories\Contracts\GatewayAggregationRepositoryInterface;
use Illuminate\Support\Carbon;

class AggregationService
{
    private $gateway_aggregation_repository;

    public function __construct()
    {
        $this->gateway_aggregation_repository = resolve(GatewayAggregationRepositoryInterface::class);
    }

    public function deposit(int $gateway, int $amount, \Carbon\Carbon $date = null)
    {
        $gatewayAggregationItem = $this->getAggregationItem($gateway, $date);
        $gatewayAggregationItem->increment('gateway_report_deposit', $amount);
    }

    public function withdrawal(int $gateway, int $amount, \Carbon\Carbon $date = null)
    {
        $gatewayAggregationItem = $this->getAggregationItem($gateway, $date);
        $gatewayAggregationItem->increment('gateway_report_withdrawal', $amount);
    }

    private function getAggregationItem(int $gateway, \Carbon\Carbon $date = null)
    {
        $aggregationDate = is_null($date) ? \Carbon\Carbon::now() : $date;

        $gatewayAggregationItem = $this->gateway_aggregation_repository->existAggregation($gateway,
            $aggregationDate->format("Y-m-d"));
        if (is_null($gatewayAggregationItem)) {
            $gatewayAggregationItem = $this->gateway_aggregation_repository->store([
                'gateway_report_gateway_id' => $gateway,
                'gateway_report_date' => $aggregationDate->format("Y-m-d")
            ]);
        }
        return $gatewayAggregationItem;
    }


}