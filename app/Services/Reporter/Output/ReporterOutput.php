<?php


namespace App\Services\Reporter\Output;


interface ReporterOutput
{
    public function output($data);
}