<?php


namespace App\Services\GatewayTransaction\Verifier\Handlers;


use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Verifier\Contracts\Verifier;
use App\Services\GatewayTransaction\Verifier\Exceptions\TimeOutException;
use Carbon\Carbon;

class TimeOutValidator extends Verifier
{

    protected function process(TransactionVerifyRequest $request)
    {
        $timeout = config('transaction.verify.timeout');
        $gatewayTransactionRepository = resolve(GatewayTransactionRepositoryInterface::class);
        $transaction =$gatewayTransactionRepository->findBy([
            'gateway_transaction_key' => $request->getTransactionKey(),
        ]);
        $now = Carbon::now();
        $timeOutTransaction = $now->diffInMinutes($transaction->updated_at);
        if($timeout < $timeOutTransaction)
        {
            throw new TimeOutException('transaction verification timed out!');
        }
        return true;
    }
}