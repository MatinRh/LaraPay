<?php


namespace App\Services\GatewayTransaction\Verifier;


use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Verifier\Handlers\AmountValidator;
use App\Services\GatewayTransaction\Verifier\Handlers\ResNumberValidator;
use App\Services\GatewayTransaction\Verifier\Handlers\TokenValidator;
use App\Services\GatewayTransaction\Verifier\Handlers\TransactionKeyValidator;

class TransactionVerifier
{
    public function validate(TransactionVerifyRequest $request)
    {
        $amountValidator = new AmountValidator();
        $resNumberValidator = new ResNumberValidator($amountValidator);
        $transactionKey = new TransactionKeyValidator($resNumberValidator);
        $tokenValidator = new TokenValidator($transactionKey);
        return $tokenValidator->handle($request);
    }
}