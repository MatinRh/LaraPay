<?php


namespace App\Services\GatewayTransaction\Validator\Handlers;


use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\GatewayTransaction\Validator\Contracts\Validator;
use App\Services\GatewayTransaction\Validator\Exceptions\InvalidAmountException;

class AmountValidator extends Validator
{

    protected function process(TransactionRequest $request)
    {
        $min = config('transaction.request.min');
        $max = config('transaction.request.max');
        if($request->getAmount() > $max || $request->getAmount() < $min)
        {
            throw new InvalidAmountException('invalid amount!');
        }
        return true;
    }
}