<?php


namespace App\Services\GatewayTransaction\Validator\Handlers;


use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\GatewayTransaction\Validator\Contracts\Validator;
use App\Services\GatewayTransaction\Validator\Exceptions\InvalidResNumberException;

class ResNumberValidator extends Validator
{

    protected function process(TransactionRequest $request)
    {
        if(strlen($request->getResNumber()) < 10)
        {
            throw new InvalidResNumberException('invalid res number!');
        }
        return true;
    }
}