<?php


namespace App\Services\GatewayTransaction;


use App\Helpers\Hash\HashGenerator;
use App\Repositories\Contracts\BankTransactionRepositoryInterface;
use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Repositories\Eloquent\Transaction\BankTransactionStatus;
use App\Repositories\Eloquent\Transaction\GatewayTransactionStatus;
use App\Services\Gateway\AggregationService;
use App\Services\GatewayTransaction\Validator\TransactionValidator;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;

class GatewayTransactionService
{
    private $gateway_transaction_repository;
    private $gateway_repository;
    private $bank_transaction_repository;

    public function __construct()
    {
        $this->gateway_transaction_repository = resolve(GatewayTransactionRepositoryInterface::class);
        $this->gateway_repository = resolve(GatewayRepositoryInterface::class);
        $this->bank_transaction_repository = resolve(BankTransactionRepositoryInterface::class);
    }

    public function make(TransactionRequest $request)
    {
        $validationResult = $this->validate($request);
        if ($validationResult) {
            return $this->create($request);
        }
    }

    private function validate(TransactionRequest $request)
    {
        $validator = new TransactionValidator();
        return $validator->validate($request);
    }

    private function create(TransactionRequest $request)
    {
        $gateway = $this->gateway_repository->findBy([
            'gateway_access_token' => $request->getToken()
        ]);
        $transactionKey = HashGenerator::make(30);
        $newTransaction = $this->gateway_transaction_repository->store([
            'gateway_transaction_gateway_id' => $gateway->gateway_id,
            'gateway_transaction_key' => $transactionKey,
            'gateway_transaction_amount' => $request->getAmount(),
            'gateway_transaction_res_number' => $request->getResNumber(),
            'gateway_transaction_description' => $request->getDescription(),
            'gateway_transaction_callback_url' => $request->getCallback(),
            'gateway_transaction_status' => GatewayTransactionStatus::PENDING
        ]);
        if ($newTransaction) {
            return $transactionKey;
        }
        return null;
    }

    public function verify(TransactionVerifyRequest $request)
    {
        //skip validation
        $transactionKey = $request->getTransactionKey();
        $amount = $request->getAmount();
        $resNumber = $request->getResNumber();
        $gatewayTransaction = $this->gateway_transaction_repository->findBy([
            'gateway_transaction_key' => $transactionKey
        ]);
        if (!$gatewayTransaction) {
            abort(404);
        }

        $bank_transaction = $gatewayTransaction->bank_transactions;
        $transactionData = $bank_transaction->bank_transaction_callback_data;
        $paymentService = new PaymentService();

        $transactionVerifyResult = $paymentService->verifyPayment($transactionData);
        $result = [];
        if (!$transactionVerifyResult['success']) {
            $this->gateway_transaction_repository->updateStatus($gatewayTransaction->gateway_transaction_id,
                GatewayTransactionStatus::FAILED);
            $this->bank_transaction_repository->updateStatus(
                $gatewayTransaction->bank_transactions->bank_transaction_id, BankTransactionStatus::FAILED
            );
            $result = [
                'success' => false
            ];
        } else {
            $gatewayAggregationService = new AggregationService();
            $gatewayAggregationService->deposit($gatewayTransaction->gateway_transaction_gateway_id,
                $gatewayTransaction->gateway_transaction_amount);
            $this->gateway_repository->incrBalance($gatewayTransaction->gateway_transaction_gateway_id,$gatewayTransaction->gateway_transaction_amount);
            $this->gateway_transaction_repository->update($gatewayTransaction->gateway_transaction_id,
                [
                    'gateway_transaction_ref_number' => $transactionVerifyResult['ref_number'], // str_random
                    'gateway_transaction_status' => GatewayTransactionStatus::COMPLETE
                ]);
            $this->bank_transaction_repository->update(
                $gatewayTransaction->bank_transactions->bank_transaction_id,
                [
                    'bank_transaction_ref_number' => $transactionVerifyResult['ref_number'],
                    'bank_transaction_status' => BankTransactionStatus::COMPLETE

                ]
            );
            $result = [
                'success' => true,
                'ref_number' => $transactionVerifyResult['ref_number']
            ];
        }
        return $result;
    }

}