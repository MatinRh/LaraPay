<?php


namespace App\Services\GatewayTransaction;


class TransactionRequest
{
    private $token;

    private $amount;

    private $ip;

    private $domain;

    private $res_number;

    private $description;

    private $callback;

    public function __construct(array $params)
    {
        $this->amount = $params['amount'];
        $this->token = $params['token'];
        $this->domain = $params['domain'];
        $this->ip = $params['ip'];
        $this->res_number = $params['res_number'];
        $this->description = $params['description'];
        $this->callback = $params['callback'];
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return mixed
     */
    public function getResNumber()
    {
        return $this->res_number;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

}