<?php


namespace App\Repositories\Eloquent\Setting;


use App\Models\Setting;
use App\Repositories\Contracts\EloquentBaseRepository;
use App\Repositories\Contracts\SettingsRepositoryInterface;

class EloquentSettingRepository extends EloquentBaseRepository implements SettingsRepositoryInterface
{
    protected $model = Setting::class;

}