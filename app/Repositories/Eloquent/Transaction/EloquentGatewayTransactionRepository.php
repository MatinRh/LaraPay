<?php


namespace App\Repositories\Eloquent\Transaction;


use App\Models\GatewayTransaction;
use App\Repositories\Contracts\EloquentBaseRepository;
use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use Carbon\Carbon;

class EloquentGatewayTransactionRepository
    extends EloquentBaseRepository
    implements GatewayTransactionRepositoryInterface
{
    protected $model = GatewayTransaction::class;

    public function updateStatus(int $id,int $status)
    {
        $item = $this->find($id);
        if(!$item)
        {
            return false;
        }
        return $item->update([
            'gateway_transaction_status' => $status
        ]);
    }
}