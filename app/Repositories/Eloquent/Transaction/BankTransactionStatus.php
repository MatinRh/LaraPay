<?php


namespace App\Repositories\Eloquent\Transaction;


class BankTransactionStatus
{
    const PENDING = 0;
    const VERIFY_WAITING = 1;
    const COMPLETE = 2;
    const FAILED = 3;

    public static function getStatuses()
    {
        return [
            self::PENDING => 'در انتظار پرداخت',
            self::VERIFY_WAITING => 'در انتظار تایید',
            self::COMPLETE => 'کامل شده',
            self::FAILED => 'نا موفق'
        ];
    }
}