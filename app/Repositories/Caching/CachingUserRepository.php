<?php


namespace App\Repositories\Caching;


use App\Entities\User\UserEntity;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Cache\Repository;

class CachingUserRepository implements UserRepositoryInterface
{
    private $userRepository;
    private $cache;
    public function __construct()
    {
        $this->userRepository = resolve(UserRepositoryInterface::class);
        $this->cache = resolve(\Illuminate\Contracts\Cache\Repository::class);
    }
    public function all(array $columns = [], array $relations = [])
    {
        return $this->cache->remember('users.all',60,function(){
            return $this->userRepository->all();
        });
    }

    public function paginate(int $page, $columns = ['*'], $pageName = 'page', int $per_page = 50)
    {
        // TODO: Implement paginate() method.
    }

    public function find(int $ID, array $columns = null)
    {
        // TODO: Implement find() method.
    }

    public function findBy(array $criteria, array $columns = null, bool $single = true)
    {
        // TODO: Implement findBy() method.
    }

    public function storeMany(array $items)
    {
        // TODO: Implement storeMany() method.
    }

    public function update(int $ID, array $item)
    {
        // TODO: Implement update() method.
    }

    public function updateBy(array $criteria, array $data)
    {
        // TODO: Implement updateBy() method.
    }

    public function delete(int $ID)
    {
        // TODO: Implement delete() method.
    }

    public function deleteBy(array $criteria)
    {
        // TODO: Implement deleteBy() method.
    }

    public function getActiveUsers()
    {
        // TODO: Implement getActiveUsers() method.
    }

    public function searchUsers(string $keyword)
    {
        // TODO: Implement searchUsers() method.
    }

    public function store(array $item): UserEntity
    {
        // TODO: Implement store() method.
    }
}