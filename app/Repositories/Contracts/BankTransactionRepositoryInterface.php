<?php


namespace App\Repositories\Contracts;


interface BankTransactionRepositoryInterface extends RepositoryInterface
{
    public function updateStatus(int $id,int $status);
}