<?php


namespace App\ValueObjects;


class Mobile
{
    private $mobile;

    public function __construct(string $mobile)
    {
        $this->setMobile($mobile);
    }

    private function setMobile(string $mobile)
    {
        if ( ! preg_match('/(09(10|11|12|13|14|15|16|17|18|19|35|36|37|38|39)[0-9]{7})/', $mobile)) {
            throw new \InvalidArgumentException('Invalid Mobile Number!');
        }
        $this->mobile = $mobile;
    }

    public function __toString()
    {
        return (string)$this->mobile;
    }
}