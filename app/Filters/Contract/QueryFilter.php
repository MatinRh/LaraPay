<?php


namespace App\Filters\Contract;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    protected $builder;
    protected $request;

    public function __construct()
    {
        $this->request = Request::capture();
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $key => $value) {
            if (!method_exists($this, $key)) {
                return;
            }
            !empty($value) ? $this->{$key}($value) : $this->{$key}();

        }
        return $this->builder;
    }

    protected function filters()
    {
        return $this->request->all();
    }

}