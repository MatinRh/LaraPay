<?php


namespace App\Filters;


use App\Filters\Contract\QueryFilter;

class UserFilters extends QueryFilter
{
    public function name($value = null)
    {
        $this->builder->where('name','LIKE',"%{$value}%");
    }
}